package com.example.aplikasipemantauancovid_19;

import java.util.Date;
import java.util.List;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;

public interface Service {
    @GET("global/")
    Call<List<ModelGlobal>> getGlobals();

    @GET("global/{date}")
    static Call<List<ModelGlobal>> getGlobal(@Path("date") Date date) {
        return null;
    }

    @GET("indonesia/")
    Call<List<ModelIndonesia>> getIndonesia();

    @GET("indonesia/{tanggal}")
    Call<List<ModelIndonesia>> getIndonesia(@Path("tanggal") Date tanggal);

    @GET("provinsi/")
    Call<List<ModelProvinsi>> getProvinsi();

    @GET("provinsi/{FID}")
    Call<List<ModelProvinsi>> getProvinsi(@Path("FID") int FID);

    @POST("global/")
    Call<ModelGlobal> addGlobal(@Body ModelGlobal global);

    @POST("indonesia/")
    Call<ModelIndonesia> addIndonesia(@Body ModelIndonesia indonesia);

    @POST("provinsi/")
    Call<ModelProvinsi> addProvinsi(@Body ModelProvinsi provinsi);

    @PUT("global/{date}")
    Call<ModelGlobal> updateGlobal(@Path("date") Date date, @Body ModelGlobal global);

    @DELETE("global/{date}")
    Call<ModelGlobal> deleteGlobal(@Path("date") Date date);

    @PUT("indonesia/{tanggal}")
    Call<ModelIndonesia> updateIndonesia(@Path("tanggal") Date tanggal, @Body ModelIndonesia indonesia);

    @DELETE("indonesia/{tanggal}")
    Call<ModelIndonesia> deleteIndonesia(@Path("tanggal") Date tanggal);

    @PUT("provinsi/{FID}")
    Call<ModelProvinsi> updateProvinsi(@Path("FID") int FID, @Body ModelProvinsi provinsi);

    @DELETE("provins/{FID}")
    Call<ModelProvinsi> deleteProvinsi(@Path("FID") int FID);


    @GET("perkembangan/")
    Call<List<ModelPerkembangan>> getPerkembangans();

    @GET("perkembangan/{perkembangan_id}")
    static Call<List<ModelPerkembangan>> getperkembangan(@Path("pertembangan_id") int perkembangan_id) {
        return null;
    }

    @POST("perkembangan/")
    Call<ModelPerkembangan> addPerkembangan(@Body ModelPerkembangan perkembangan);

    @PUT("perkembangan/{perkembangan_id}")
    Call<ModelPerkembangan> updatePerkembangan(@Path("perkembangan_id") int perkembangan_id, @Body ModelPerkembangan perkembangan);

    @DELETE("perkembangan/{perkembangan_id}")
    Call<ModelPerkembangan> deletePerkembangan(@Path("perkembangan_id") int perkembangan_id);


    @GET("pengecekan/")
    Call<List<ModelPengecekan>> getPengecekans();

    @GET("pengecekan/{pengecekan_id}")
    static Call<List<ModelPengecekan>> getPengecekan(@Path("pengecekan_id") int pengecekan_id) {
        return null;
    }

    @POST("pengecekan/")
    Call<ModelPengecekan> addpengecekan(@Body ModelPengecekan pengecekan);

    @PUT("pengecekan/{pengecekan_id}")
    Call<ModelPengecekan> updatePengecekan(@Path("pengecekan_id") int pengecekan_id, @Body ModelPengecekan pengecekan);

    @DELETE("pengecekan/{pengecekan_id}")
    Call<ModelPengecekan> deletePengecekan(@Path("pengecekan_id") int pengecekan_id);


    @GET("laporan/")
    Call<List<ModelLaporan>> getLaporans();

    @GET("laporan/{laporan_id}")
    static Call<List<ModelLaporan>> getLaporan(@Path("laporan_id") int laporan_id) {
        return null;
    }

    @POST("laporan/")
    Call<ModelLaporan> addLaporan(@Body ModelLaporan laporan);

    @PUT("laporan/{laporan_id}")
    Call<ModelLaporan> updateLaporan(@Path("laporan_id") int laporan_id, @Body ModelLaporan laporan);

    @DELETE("laporan/{laporan_id}")
    Call<ModelLaporan> deleteLaporan(@Path("laporan_id") int laporan_id);

}
