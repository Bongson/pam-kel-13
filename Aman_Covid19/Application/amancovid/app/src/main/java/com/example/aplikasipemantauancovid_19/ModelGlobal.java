package com.example.aplikasipemantauancovid_19;

import androidx.annotation.NonNull;

import com.google.gson.annotations.SerializedName;

import java.util.Date;

public class ModelGlobal {
    @SerializedName("date")
    private Date date;
    @SerializedName("positif")
    private int positif;
    @SerializedName("sembuh")
    private int sembuh;
    @SerializedName("meninggal")
    private int meninggal;

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public int getPositif() {
        return positif;
    }

    public void setPositif(int positif) {
        this.positif = positif;
    }

    public int getSembuh() {
        return sembuh;
    }

    public void setSembuh(int sembuh) {
        this.sembuh = sembuh;
    }

    public int getMeninggal() {
        return meninggal;
    }

    public void setMeninggal(int meninggal) {
        this.meninggal = meninggal;
    }

    @Override
    public String toString() {
        return "ModelGlobal{" +
                "date=" + date +
                ", positif=" + positif +
                ", sembuh=" + sembuh +
                ", meninggal=" + meninggal +
                '}';
    }
}
