package com.example.aplikasipemantauancovid_19;

import com.google.gson.annotations.SerializedName;

import java.util.Date;

public class ModelPerkembangan {
    @SerializedName("perkembangan_id")
    private int perkembangan_id;

    @SerializedName("perkembangan_judul")
    private String perkembangan_judul;

    @SerializedName("perkembangan_isi")
    private String perkembangan_isi;

    @SerializedName("perkembangan_post")
    private Date perkembangan_post;

    public int getPerkembangan_id() {
        return perkembangan_id;
    }

    public void setPerkembangan_id(int perkembangan_id) {
        this.perkembangan_id = perkembangan_id;
    }

    public String getPerkembangan_judul() {
        return perkembangan_judul;
    }

    public void setPerkembangan_judul(String perkembangan_judul) {
        this.perkembangan_judul = perkembangan_judul;
    }

    public String getPerkembangan_isi() {
        return perkembangan_isi;
    }

    public void setPerkembangan_isi(String perkembangan_isi) {
        this.perkembangan_isi = perkembangan_isi;
    }

    public Date getPerkembangan_post() {
        return perkembangan_post;
    }

    public void setPerkembangan_post(Date perkembangan_post) {
        this.perkembangan_post = perkembangan_post;
    }

    @Override
    public String toString() {
        return "ModelPerkembangan{" +
                "perkembangan_id=" + perkembangan_id +
                ", perkembangan_judul='" + perkembangan_judul + '\'' +
                ", perkembangan_isi='" + perkembangan_isi + '\'' +
                ", perkembangan_post=" + perkembangan_post +
                '}';
    }
}
