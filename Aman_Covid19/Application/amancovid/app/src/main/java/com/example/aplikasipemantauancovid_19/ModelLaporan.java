package com.example.aplikasipemantauancovid_19;

import com.google.gson.annotations.SerializedName;

public class ModelLaporan {
    @SerializedName("laporan_id")
    private int laporan_id;

    @SerializedName("laporan_nama")
    private String laporan_nama;

    @SerializedName("laporan_alamat")
    private String laporan_alamat;

    @SerializedName("pelapor_id")
    private int pelapor_id;

    public int getLaporan_id() {
        return laporan_id;
    }

    public void setLaporan_id(int laporan_id) {
        this.laporan_id = laporan_id;
    }

    public String getLaporan_nama() {
        return laporan_nama;
    }

    public void setLaporan_nama(String laporan_nama) {
        this.laporan_nama = laporan_nama;
    }

    public String getLaporan_alamat() {
        return laporan_alamat;
    }

    public void setLaporan_alamat(String laporan_alamat) {
        this.laporan_alamat = laporan_alamat;
    }

    public int getPelapor_id() {
        return pelapor_id;
    }

    public void setPelapor_id(int pelapor_id) {
        this.pelapor_id = pelapor_id;
    }

    @Override
    public String toString() {
        return "ModelLaporan{" +
                "laporan_id=" + laporan_id +
                ", laporan_nama='" + laporan_nama + '\'' +
                ", laporan_alamat='" + laporan_alamat + '\'' +
                ", pelapor_id=" + pelapor_id +
                '}';
    }
}
