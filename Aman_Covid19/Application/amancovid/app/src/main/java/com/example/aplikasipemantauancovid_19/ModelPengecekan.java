package com.example.aplikasipemantauancovid_19;

import com.google.gson.annotations.SerializedName;

import java.util.Date;

public class ModelPengecekan {
    @SerializedName("pengecekan_id")
    private int pengecekan_id;

    @SerializedName("pengecekan_user_id")
    private int pengecekan_user_id;

    @SerializedName("pengecekan_tanggal")
    private Date pengecekan_tanggal;

    @SerializedName("pengecekann_yes")
    private int pengecekan_yes;

    @SerializedName("pengecekan_no")
    private int pengecekan_no;

    public int getPengecekan_id() {
        return pengecekan_id;
    }

    public void setPengecekan_id(int pengecekan_id) {
        this.pengecekan_id = pengecekan_id;
    }

    public int getPengecekan_user_id() {
        return pengecekan_user_id;
    }

    public void setPengecekan_user_id(int pengecekan_user_id) {
        this.pengecekan_user_id = pengecekan_user_id;
    }

    public Date getPengecekan_tanggal() {
        return pengecekan_tanggal;
    }

    public void setPengecekan_tanggal(Date pengecekan_tanggal) {
        this.pengecekan_tanggal = pengecekan_tanggal;
    }

    public int getPengecekan_yes() {
        return pengecekan_yes;
    }

    public void setPengecekan_yes(int pengecekan_yes) {
        this.pengecekan_yes = pengecekan_yes;
    }

    public int getPengecekan_no() {
        return pengecekan_no;
    }

    public void setPengecekan_no(int pengecekan_no) {
        this.pengecekan_no = pengecekan_no;
    }

    @Override
    public String toString() {
        return "ModelPengecekan{" +
                "pengecekan_id=" + pengecekan_id +
                ", pengecekan_user_id=" + pengecekan_user_id +
                ", pengecekan_tanggal=" + pengecekan_tanggal +
                ", pengecekan_yes=" + pengecekan_yes +
                ", pengecekan_no=" + pengecekan_no +
                '}';
    }
}
