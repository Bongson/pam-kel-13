package com.example.aplikasipemantauancovid_19;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;

import android.annotation.SuppressLint;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DashBoard extends AppCompatActivity {
    Button signout, check;
    CardView datakasus, laporankasus, perkembangan, pengecekan;
    ListView listView;

    Service service;
    List<ModelGlobal> globals = new ArrayList<ModelGlobal>();
    List<ModelIndonesia> indonesia = new ArrayList<ModelIndonesia>();
    List<ModelProvinsi> provinsiList = new ArrayList<ModelProvinsi>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dash_board);

        signout = findViewById(R.id.signout);
        check = findViewById(R.id.cek_daily);
        datakasus = findViewById(R.id.datakasus);
        laporankasus = findViewById(R.id.laporankasus);
        perkembangan = findViewById(R.id.perkembangan);
        pengecekan = findViewById(R.id.pengecekan);

        service = APIUtils.getService();

        Date c = Calendar.getInstance().getTime();
        Toast.makeText(this, "Current Time => " + c, Toast.LENGTH_SHORT).show();

        @SuppressLint("SimpleDateFormat") SimpleDateFormat df = new SimpleDateFormat("yyyy.MM.dd G 'at' HH:mm:ss z");
        String formattedDate = df.format(c);

        //getList(c);

        signout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new AlertDialog.Builder(DashBoard.this).setMessage("Apakah Anda Yakin ingin Keluar ?").setCancelable(true)
                        .setPositiveButton("Ya", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Intent signout = new Intent(DashBoard.this, LoginActivity.class);
                        startActivity(signout);
                        finish();
                    }
                }).setNegativeButton("Tidak",null).show();
            }
        });

        perkembangan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent perkembangan = new Intent(DashBoard.this, PerkembanganKasus.class);
                startActivity(perkembangan);
            }
        });

        laporankasus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent perkembangan = new Intent(DashBoard.this, LaporanKasus.class);
                startActivity(perkembangan);
            }
        });

        datakasus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent perkembangan = new Intent(DashBoard.this, DataKasus.class);
                startActivity(perkembangan);
            }
        });

        pengecekan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent perkembangan = new Intent(DashBoard.this, Pengecekan.class);
                startActivity(perkembangan);
            }
        });
    }
/*
    private void getList(Date c) {
        Call<List<ModelGlobal>> call1 = Service.getGlobal(c);
        call1.enqueue(new Callback<List<ModelGlobal>>() {
            @Override
            public void onResponse(Call<List<ModelGlobal>> call, Response<List<ModelGlobal>> response) {
                if (response.isSuccessful()){
                    globals = response.body();
                    //listView.setAdapter(new GlobalAdapter(DashBoard.this,R.layout.list_global,listView));
                }
            }

            @Override
            public void onFailure(Call<List<ModelGlobal>> call, Throwable t) {

            }
        });
    }

 */
}