package com.example.aplikasipemantauancovid_19;

public class APIUtils {

    private APIUtils(){
    };

    public static final String API_URL = "http://localhost:8080/api/records/";

    public static Service getService(){
        return RetrofitClient.getClient(API_URL).create(Service.class);
    }

}
