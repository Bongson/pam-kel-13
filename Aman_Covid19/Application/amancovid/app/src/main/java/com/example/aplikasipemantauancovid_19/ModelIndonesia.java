package com.example.aplikasipemantauancovid_19;

import androidx.annotation.NonNull;

import com.google.gson.annotations.SerializedName;

import java.util.Date;

public class ModelIndonesia {
    @SerializedName("tanggal")
    private Date tanggal;
    @SerializedName("positif")
    private int positif;
    @SerializedName("sembuh")
    private int sembuh;
    @SerializedName("meninggal")
    private int meninggal;

    public Date getTanggal() {
        return tanggal;
    }

    public void setTanggal(Date tanggal) {
        this.tanggal = tanggal;
    }

    public int getPositif() {
        return positif;
    }

    public void setPositif(int positif) {
        this.positif = positif;
    }

    public int getSembuh() {
        return sembuh;
    }

    public void setSembuh(int sembuh) {
        this.sembuh = sembuh;
    }

    public int getMeninggal() {
        return meninggal;
    }

    public void setMeninggal(int meninggal) {
        this.meninggal = meninggal;
    }

    @Override
    public String toString() {
        return "ModelIndonesia{" +
                "tanggal=" + tanggal +
                ", positif=" + positif +
                ", sembuh=" + sembuh +
                ", meninggal=" + meninggal +
                '}';
    }
}
