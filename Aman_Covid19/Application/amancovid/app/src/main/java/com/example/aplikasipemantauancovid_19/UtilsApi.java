package com.example.aplikasipemantauancovid_19;

public class UtilsApi {
    public static final String BASE_URL_API = "http://192.168.43.75:8080/Web%20Service/aman_covid19/public/";

    // Mendeklarasikan Interface BaseApiService
    public static BaseApiService getAPIService(){
        return RetrofitClient.getClient(BASE_URL_API).create(BaseApiService.class);
    }
}
