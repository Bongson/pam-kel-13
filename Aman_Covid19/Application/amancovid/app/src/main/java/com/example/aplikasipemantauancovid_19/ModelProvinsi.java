package com.example.aplikasipemantauancovid_19;

import androidx.annotation.NonNull;

import com.google.gson.annotations.SerializedName;

public class ModelProvinsi {
    @SerializedName("FID")
    private int FID;

    @SerializedName("Kode_Provi")
    private int Kode_Profi;

    @SerializedName("Provinsi")
    private String Provinsi;

    @SerializedName("Kasus_Posi")
    private int Kasus_Posi;

    @SerializedName("Kasus_Semb")
    private int Kasus_Semb;

    @SerializedName("Kasus_Meni")
    private int Kasus_Meni;

    public int getFID() {
        return FID;
    }

    public void setFID(int FID) {
        this.FID = FID;
    }

    public int getKode_Profi() {
        return Kode_Profi;
    }

    public void setKode_Profi(int kode_Profi) {
        Kode_Profi = kode_Profi;
    }

    public String getProvinsi() {
        return Provinsi;
    }

    public void setProvinsi(String provinsi) {
        Provinsi = provinsi;
    }

    public int getKasus_Posi() {
        return Kasus_Posi;
    }

    public void setKasus_Posi(int kasus_Posi) {
        Kasus_Posi = kasus_Posi;
    }

    public int getKasus_Semb() {
        return Kasus_Semb;
    }

    public void setKasus_Semb(int kasus_Semb) {
        Kasus_Semb = kasus_Semb;
    }

    public int getKasus_Meni() {
        return Kasus_Meni;
    }

    public void setKasus_Meni(int kasus_Meni) {
        Kasus_Meni = kasus_Meni;
    }

    @Override
    public String toString() {
        return "ModelProvinsi{" +
                "FID=" + FID +
                ", Kode_Profi=" + Kode_Profi +
                ", Provinsi='" + Provinsi + '\'' +
                ", Kasus_Posi=" + Kasus_Posi +
                ", Kasus_Semb=" + Kasus_Semb +
                ", Kasus_Meni=" + Kasus_Meni +
                '}';
    }
}
